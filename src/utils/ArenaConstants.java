/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package utils;

/**
 *
 * @author lennovo
 */
public class ArenaConstants {
    public static final String ARENA = "Arena";
    public static final Integer LENGTH = 15;
    public static final Integer BREADTH = 15;
    public static final Float HEIGHT = 0.2f;
    public static final Integer ArenaTranslationX = 0;
    public static final Integer ArenaTranslationY = -4;
    public static final Integer ArenaTranslationZ = -5;
    public static final String MATERIAL_RESOURCE_PATH = "Common/MatDefs/Misc/Unshaded.j3md";
    public static final String MAT_COLOR = "Color";

}
