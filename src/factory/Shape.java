/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package factory;

import com.jme3.bullet.control.RigidBodyControl;
import com.jme3.scene.Geometry;

/**
 *
 * @author lennovo
 */
public interface Shape {
    public Geometry getGeometryObject();
    public RigidBodyControl getRBCObject();
    
}
