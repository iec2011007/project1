/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package driver;

import com.jme3.app.SimpleApplication;
import com.jme3.bullet.BulletAppState;
import com.jme3.bullet.collision.shapes.CapsuleCollisionShape;
import com.jme3.bullet.collision.shapes.CollisionShape;
import com.jme3.bullet.control.CharacterControl;
import com.jme3.bullet.control.RigidBodyControl;
import com.jme3.bullet.util.CollisionShapeFactory;
import com.jme3.math.Vector3f;
import com.jme3.scene.Spatial;
import modals.Arena;

/**
 *
 * @author lennovo
 */
public class Driver extends SimpleApplication{

    private BulletAppState BAS;
    private Spatial terrain;
    private RigidBodyControl terrainRBC;
    private CharacterControl player;
    
    public static void main1(String[] args) {
        Driver app = new Driver();
        app.start();
    }
    
    @Override
    public void simpleInitApp() {
       //super.rootNode.attachChild(new Arena().makeFloor(super.assetManager));
       this.BAS = new BulletAppState();
       stateManager.attach(BAS);
       
       terrain = new Arena().makeFloor(super.assetManager);
        CollisionShape cs = CollisionShapeFactory.createMeshShape(terrain);
        
        terrainRBC = new RigidBodyControl(cs, 0);
        
        CapsuleCollisionShape playerC = new CapsuleCollisionShape(1.5f, 6f, 1);
        player = new CharacterControl(playerC, 0.05f);
        player.setFallSpeed(5f);
        player.setJumpSpeed(5f);
        player.setGravity(30);
        player.setPhysicsLocation(new Vector3f(0,50,0));
        
        BAS.getPhysicsSpace().add(terrainRBC);
        BAS.getPhysicsSpace().add(player);
        rootNode.attachChild(terrain);
    }
    
    @Override
    public void simpleUpdate(float tpf) {
        cam.setLocation(player.getPhysicsLocation());
    }
        
    
}
