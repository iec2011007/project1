package driver;

/**
 *
 * @author lennovo
 */
import characters.MyGameCharacterControl;
import com.jme3.app.SimpleApplication;
import com.jme3.asset.plugins.ZipLocator;
import com.jme3.bullet.BulletAppState;
import com.jme3.bullet.collision.shapes.CapsuleCollisionShape;
import com.jme3.bullet.collision.shapes.CollisionShape;
import com.jme3.bullet.collision.shapes.CylinderCollisionShape;
import com.jme3.bullet.collision.shapes.SphereCollisionShape;
import com.jme3.bullet.control.CharacterControl;
import com.jme3.bullet.control.RigidBodyControl;
import com.jme3.bullet.util.CollisionShapeFactory;
import com.jme3.collision.CollisionResults;
import com.jme3.font.BitmapText;
import com.jme3.input.KeyInput;
import com.jme3.input.controls.ActionListener;
import com.jme3.input.controls.KeyTrigger;
import com.jme3.light.AmbientLight;
import com.jme3.light.DirectionalLight;
import com.jme3.material.Material;
import com.jme3.math.ColorRGBA;
import com.jme3.math.Ray;
import com.jme3.math.Triangle;
import com.jme3.math.Vector3f;
import com.jme3.scene.Geometry;
import com.jme3.scene.Node;
import com.jme3.scene.Spatial;
import com.jme3.scene.shape.Box;
import com.jme3.scene.shape.Cylinder;
import com.jme3.scene.shape.Sphere;
import java.util.ArrayList;
import modals.BoxObject;
import modals.SpatialState;
import modals.SphereObject;
import utils.ObjectCreator;

public class HelloCollision extends SimpleApplication
        implements ActionListener {

    private Vector3f normalGravity = new Vector3f(0, -9.81f, 0);
    private Spatial sceneModel;
    private BulletAppState bulletAppState;
    private RigidBodyControl landscape;
    private CharacterControl player;
    private Vector3f walkDirection = new Vector3f();
    private boolean left = false, right = false, up = false, down = false;
    private static CollisionResults collisionResults;
    private static Ray ray;
    private ArrayList<SpatialState> gameState;
    private SpatialState activeState;
    private static BitmapText helloText;
    private static int countHits = 0;
    //Temporary vectors used on each frame.
    //They here to avoid instanciating new vectors on each frame
    private Vector3f camDir = new Vector3f();
    private Vector3f camLeft = new Vector3f();
    private static Node playerNode;

    public static void main(String[] args) {
        HelloCollision app = new HelloCollision();
        app.start();
    }

    public void simpleInitApp() {
        /**
         * Set up text
         */
        guiNode.detachAllChildren();
        guiFont = assetManager.loadFont("Interface/Fonts/Default.fnt");
        helloText = new BitmapText(guiFont, false);
        helloText.setSize(guiFont.getCharSet().getRenderedSize());
        helloText.setText("Hit");
        helloText.setLocalTranslation(300, helloText.getLineHeight(), 0);

        /**
         * Set up Physics
         */
        bulletAppState = new BulletAppState();
        stateManager.attach(bulletAppState);
        //bulletAppState.getPhysicsSpace().enableDebug(assetManager);

        // We re-use the flyby camera for rotation, while positioning is handled by physics
        viewPort.setBackgroundColor(new ColorRGBA(0.7f, 0.8f, 1f, 1f));
        flyCam.setMoveSpeed(100);
        setUpKeys();
        setUpLight();

        // We load the scene from the zip file and adjust its size.
        assetManager.registerLocator("town.zip", ZipLocator.class);
        sceneModel = assetManager.loadModel("main.scene");
        sceneModel.setLocalScale(2f);

        // We set up collision detection for the scene by creating a
        // compound collision shape and a static RigidBodyControl with mass zero.
        CollisionShape sceneShape
                = CollisionShapeFactory.createMeshShape(sceneModel);
        landscape = new RigidBodyControl(sceneShape, 0);
        sceneModel.addControl(landscape);

        // We set up collision detection for the player by creating
        // a capsule collision shape and a CharacterControl.
        // The CharacterControl offers extra settings for
        // size, stepheight, jumping, falling, and gravity.
        // We also put the player in its starting position.
        CapsuleCollisionShape capsuleShape = new CapsuleCollisionShape(1.5f, 6f, 1);
        player = new CharacterControl(capsuleShape, 0.05f);
        player.setJumpSpeed(20);
        player.setFallSpeed(30);
        player.setGravity(30);
        player.setPhysicsLocation(new Vector3f(0, 10, 0));

        // We attach the scene and the player to the rootnode and the physics space,
        // to make them appear in the game world.
        rootNode.attachChild(sceneModel);
        bulletAppState.getPhysicsSpace().add(landscape);
        bulletAppState.getPhysicsSpace().add(player);

        setupMoveableObjects();

    }

    private void setupMoveableObjects() {
        SpatialState box = ObjectCreator.createBox(assetManager);
        RigidBodyControl boxRBC = box.getRbc();
        bulletAppState.getPhysicsSpace().add(boxRBC);
        boxRBC.setPhysicsLocation(new Vector3f(0, 10, 10));
        //rootNode.attachChild(box.getGeom()); //Since Active

        SpatialState sphere = ObjectCreator.createSphere(super.assetManager);
        RigidBodyControl srbc = sphere.getRbc();
        bulletAppState.getPhysicsSpace().add(srbc);
        srbc.setPhysicsLocation(new Vector3f(80, 10, 10));
        rootNode.attachChild(sphere.getGeom());

        SpatialState cylinder = ObjectCreator.createCylinder(assetManager);
        RigidBodyControl csrbc = cylinder.getRbc();
        bulletAppState.getPhysicsSpace().add(csrbc);
        csrbc.setPhysicsLocation(new Vector3f(-130, 10, 0));
        rootNode.attachChild(cylinder.getGeom());

        SpatialState ninja = ObjectCreator.createNinja(assetManager);
        RigidBodyControl ninjarbc = ninja.getRbc();
        bulletAppState.getPhysicsSpace().add(ninjarbc);
        ninjarbc.setPhysicsLocation(new Vector3f(90, 7, 7));
        rootNode.attachChild(ninja.getGeom());
        
        SpatialState devil = ObjectCreator.createDevil(assetManager);
        RigidBodyControl devilrbc = devil.getRbc();
        bulletAppState.getPhysicsSpace().add(devilrbc);
        devilrbc.setPhysicsLocation(new Vector3f(50, 7, 7));
        rootNode.attachChild(devil.getGeom());
        
        
        gameState = new ArrayList<>();
        gameState.add(box);
        gameState.add(sphere);
        gameState.add(cylinder);
        gameState.add(ninja);
        gameState.add(devil);
        
//        SpatialState ss = new SpatialState(new Integer(3), playerNode,
//                charControl.getRbc(), new Vector3f(30, 10, 20));
//        gameState.add(ss);
        activeState = gameState.get(0);
    }

    private void setUpLight() {
        // We add light so we see the scene
        AmbientLight al = new AmbientLight();
        al.setColor(ColorRGBA.White.mult(1.3f));
        rootNode.addLight(al);

        DirectionalLight dl = new DirectionalLight();
        dl.setColor(ColorRGBA.White);
        dl.setDirection(new Vector3f(2.8f, -2.8f, -2.8f).normalizeLocal());
        rootNode.addLight(dl);
    }

    /**
     * We over-write some navigational key mappings here, so we can add
     * physics-controlled walking and jumping:
     */
    private void setUpKeys() {
        inputManager.addMapping("Left", new KeyTrigger(KeyInput.KEY_A));
        inputManager.addMapping("Right", new KeyTrigger(KeyInput.KEY_D));
        inputManager.addMapping("Up", new KeyTrigger(KeyInput.KEY_W));
        inputManager.addMapping("Down", new KeyTrigger(KeyInput.KEY_S));
        inputManager.addMapping("Jump", new KeyTrigger(KeyInput.KEY_SPACE));
        inputManager.addMapping("1", new KeyTrigger(KeyInput.KEY_1));
        inputManager.addMapping("2", new KeyTrigger(KeyInput.KEY_2));
        inputManager.addMapping("3", new KeyTrigger(KeyInput.KEY_3));
        inputManager.addMapping("4", new KeyTrigger(KeyInput.KEY_4));
        inputManager.addMapping("5", new KeyTrigger(KeyInput.KEY_5));
        inputManager.addListener(this, "Left");
        inputManager.addListener(this, "Right");
        inputManager.addListener(this, "Up");
        inputManager.addListener(this, "Down");
        inputManager.addListener(this, "Jump");
        inputManager.addListener(this, "1");
        inputManager.addListener(this, "2");
        inputManager.addListener(this, "3");
        inputManager.addListener(this, "4");
        inputManager.addListener(this, "5");
    }

    /**
     * These are our custom actions triggered by key presses. We do not walk
     * yet, we just keep track of the direction the user pressed.
     */
    public void onAction(String binding, boolean isPressed, float tpf) {
        countHits++;
        ray = new Ray(cam.getLocation(), cam.getDirection());
        collisionResults = new CollisionResults();
        rootNode.collideWith(ray, collisionResults);
        System.out.println("result size " + collisionResults.size() + " coutn hits = " + countHits);
        if (collisionResults.size() > 0) {
            String collidedNodeName = collisionResults.getCollision(0).getGeometry().getName();
            System.out.println("name = "+ collidedNodeName);
            if (collidedNodeName.equals("S") ||
                    collidedNodeName.equals("C") || 
                    collidedNodeName.equals("Box") ||
                    collidedNodeName.equals("Oto-geom-1") || 
                    collidedNodeName.equals("level-geom-2") || 
                    collidedNodeName.equals("Ninja-geom-2") 
                    
                    ) {
                guiNode.attachChild(helloText);
                System.out.println("result value  " + collisionResults.getCollision(0).getGeometry().getName());
            }
        }

        if (countHits > 3) {
            countHits = 0;
            guiNode.detachChild(helloText);
        }
        //bulletAppState
        if (binding.equals("Left")) {
            left = isPressed;
        } else if (binding.equals("Right")) {
            right = isPressed;
        } else if (binding.equals("Up")) {
            up = isPressed;
        } else if (binding.equals("Down")) {
            down = isPressed;
        } else if (binding.equals("Jump")) {
            if (isPressed) {
                player.jump();
            }
        } else {
            if ((activeState.getId()).equals(Integer.parseInt(binding) - 1) && isPressed) {
                activeState.setCurrentPos(player.getPhysicsLocation());
            } else {
                if (isPressed) {
                    Vector3f playerCurrPos = player.getPhysicsLocation();
                    Integer currentId = activeState.getId();
                    SpatialState currentState = gameState.get(currentId);

                    currentState.setCurrentPos(playerCurrPos);

                    RigidBodyControl currentRBC = currentState.getRbc();
                    currentRBC.setPhysicsLocation(playerCurrPos);
                    currentState.setRbc(currentRBC);
                    rootNode.attachChild(currentState.getGeom());

                    //currentState.getRbc().setPhysicsLocation(currentState.getCurrentPos());
                    gameState.set(currentId, currentState);
                    Integer nextStateId = Integer.parseInt(binding) - 1;

                    rootNode.detachChild(gameState.get(nextStateId).getGeom());

                    activeState = gameState.get(nextStateId);
                    player.setPhysicsLocation(activeState.getCurrentPos());
                }
            }
        }
    }

    /**
     * This is the main event loop--walking happens here. We check in which
     * direction the player is walking by interpreting the camera direction
     * forward (camDir) and to the side (camLeft). The setWalkDirection()
     * command is what lets a physics-controlled player walk. We also make sure
     * here that the camera moves with player.
     */
    @Override
    public void simpleUpdate(float tpf) {
        camDir.set(cam.getDirection()).multLocal(0.6f);
        camLeft.set(cam.getLeft()).multLocal(0.4f);
        walkDirection.set(0, 0, 0);
        if (left) {
            walkDirection.addLocal(camLeft);
        }
        if (right) {
            walkDirection.addLocal(camLeft.negate());
        }
        if (up) {
            walkDirection.addLocal(camDir);
        }
        if (down) {
            walkDirection.addLocal(camDir.negate());
        }
        player.setWalkDirection(walkDirection);
        cam.setLocation(player.getPhysicsLocation());
    }
}
