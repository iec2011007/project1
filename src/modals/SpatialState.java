/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package modals;

import com.jme3.bullet.control.RigidBodyControl;
import com.jme3.math.Vector3f;
import com.jme3.scene.Geometry;
import com.jme3.scene.Spatial;
import factory.Shape;

/**
 *
 * @author lennovo
 */
public class SpatialState {
    private Integer id;
    private Spatial geom;
    private RigidBodyControl rbc;
    private Vector3f currentPos;

    public SpatialState(Integer id, Spatial geom, RigidBodyControl rbc, Vector3f _cPos) {
        this.id = id;
        this.geom = geom;
        this.rbc = rbc;
        this.currentPos = _cPos;
    }

    public Vector3f getCurrentPos() {
        return currentPos;
    }

    public void setCurrentPos(Vector3f currentPos) {
        this.currentPos = currentPos;
    }
    
    

    public Integer getId() {
        return id;
    }

    public Spatial getGeom() {
        return geom;
    }

    public RigidBodyControl getRbc() {
        return rbc;
    }

    public void setGeom(Geometry geom) {
        this.geom = geom;
    }

    public void setRbc(RigidBodyControl rbc) {
        this.rbc = rbc;
    }
    
    
    
}
